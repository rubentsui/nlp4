<img src="images/gpti.gif" width="50%" align="right">

Welcome To the GPTI Translation Tech Toolkit
============================================

**Chinese word segmentation and POS tagging**:
 - `CKIP` (Academia Sinica) neural network-based tagger;
 - ICTCLAS `PyNLPIR`
 
**English POS tagging**: `SpaCy`

**Simplified <-> Traditional Chinese conversion**: `OpenCC`

**Word Alignment**: `Anymalign`

Getting Started
--
To get started, double click on the **`Update.ipynb`** notebook to retrieve the latest version of the toolkit.

References
--
 - [CkipTagger](https://github.com/ckiplab/ckiptagger)
 - [PyNLPIR](https://pynlpir.readthedocs.io/en/latest/tutorial.html)
 - [SpaCy](https://spacy.io/)

